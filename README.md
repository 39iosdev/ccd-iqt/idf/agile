# How to Ask Questions

In preface to the Agile unit, there is a discussion of **how to ask questions** [here](./mdbook/src/Agile/howtoaskquestions/Howtoaskquestions.html)
  
# Agile

**The Agile Overview MDbook is [here](./mdbook/src/Agile/Overview.html)**  

**The slides are available [here](./mdbook/src/Agile/slides/#/)**  

![A map of Agile](./mdbook/src/assets/agilesubway.png)  

Agile means to be flexible.  
Modern software development relies on a fast, transparent process.
The emphasis is on creating viable products as fast as possible - so that they may then be improved.  

Agile is a *Fail fast* approach to software development.

**Sources**

* For more complete topic content you may refer to original content and further references at :  
  * [The Agile Alliance](https://www.agilealliance.org/agile101/)  
  * [The Scrum Alliance](https://www.scrumalliance.org/about-scrum/overview)  

#### To access the Agile slides please click [here](./slides)  
