# How to Ask Questions  

In preparation for the Agile unit, there is a discussion of **how to ask questions** [here](https://39iosdev.gitlab.io/ccd-iqt/idf/agile/howtoaskquestions/Howtoaskquestions.html)  

# Agile

**The Agile Overview MDbook is** [here](https://39iosdev.gitlab.io/ccd-iqt/idf/agile/Agile/Overview.html)  

**The *slides*** are available [here](https://39iosdev.gitlab.io/ccd-iqt/idf/agile/slides/#/)  

[The Agile Umbrella](https://www.visual-paradigm.com/scrum/scrum-vs-waterfall-vs-agile-vs-lean-vs-kanban/)  

![Agile is the overarching framework](./assets/agile-umbrella.png)  

Agile means to be flexible.
Modern software development relies on a fast, transparent process.
The emphasis is on creating viable products as fast as possible - so that they may then be improved.

Agile is a *Fail fast* approach to software development.

---

**Sources**

* For more complete topic content you may refer to original content and further references at :
  * [The Agile Alliance](https://www.agilealliance.org/agile101/)  
  * [The Scrum Alliance](https://www.scrumalliance.org/about-scrum/overview)  

#### To access the Agile slides please click [here](./slides)
