
# Question Scavenger Hunt

## In 5 minutes, try to find URLs to answer as many of these as you can:
# Place the URL after the `->`
1. What is the logo for the 39th?
`->`
2. Where can you find low quality / unanswered questions about programming?
`->`
3. What are the parts of a Fernet key in Python?
`->`
4. What is the '-->' for in C?
`->`
5. What is the difference between a `set` and a `tuple` in Python?
`->`
6. Who created Python?
`->`
7. How can you find if a variable has been used in a running Python program?
`->`
8. What is the best implementation of Agile?
`->`
9. What is a scrum master?
`->`
10. How do you get the exclusive OR values of two octal numbers in Python?
`->`
11. What is the best webcomic for programmers?
`->`
12. Where can you find the master training task list for the 90th?
`->`
13. Where can you find the running pipelines for the 90th?
`->`
14. How do you list all files that contain specific text on a Linux computer?
`->`
15. How do you use `grep` in Windows?
`->`
