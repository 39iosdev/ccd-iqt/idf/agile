[How to Ask Questions](http://www.catb.org/~esr/faqs/smart-questions.html)  

---

## How to Ask Questions

**Specifically, the hows of "How to ask your question"**  

### Checklist to ask a question

- [X] Do you have a question?  
- [ ] Create a meaningful, specific subject header  
- [ ] Does the header describe your problem?  
- [ ] Does the header have enough detail to separate it from similar questions?
- [ ] Could someone with the same question find your question from the subject?  
    - [ ] Would they find it **quickly**?
- [ ] Write a question that is:
    - [ ] *clear*
    - [ ] *grammatically correct*  
    - [ ] *spelled correctly*
- [ ] Provide an *easily* reproducible code example
- [ ] Is there ***enough of an example*** to reproduce your question?
    - [ ] Have any extraneous code details been eliminated?  


---
*Flowchart available [here](./assets/howtoaskaquestion.png)*


**See Also**:  

* [Stack Overflow's - How to ask a good question](https://stackoverflow.com/help/how-to-ask)  

* [RTFM](http://www.catb.org/jargon/html/R/RTFM.html)  

* [STFW](https://lmgtfy.com/?q=STFW)  

For further reference:  
[Eternal September](https://en.wikipedia.org/wiki/Eternal_September)  
