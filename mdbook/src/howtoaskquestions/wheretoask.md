[How to Ask Questions](http://www.catb.org/~esr/faqs/smart-questions.html)  

---

## **Specifically, Where should you ask your question?**  

- [x] Do you have a question?
- [x] Did you [check](./areyouready.md) that you are ready to ask your question?
- [ ] Is your question about general computing?  
    - &#10132;  Use [Super User](https://superuser.com/)
- [ ] Is your question focused on programming?
    - &#10132;  Use [Stack Overflow](https://stackoverflow.com/)
- [ ] Is your question vendor specific? 
    - &#10132; Look for more specific forum
- [ ] Is your question about servers?  
    - &#10132;  Use [Server Fault](https://serverfault.com/)
- [ ] Is your question about networking?  
    - &#10132;  Use [Server Fault](https://serverfault.com/)

---

*Flowchart available [here](./assets/wheretoaskyourquestion.png)*


**See Also**:  

* [Stack Overflow's - How to ask a good question](https://stackoverflow.com/help/how-to-ask)  

* [RTFM](http://www.catb.org/jargon/html/R/RTFM.html)  

* [STFW](https://lmgtfy.com/?q=STFW)  

For further reference:  
[Eternal September](https://en.wikipedia.org/wiki/Eternal_September)  
