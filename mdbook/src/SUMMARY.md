# Agile

[C-4102L-TM Agile](README.md)

- [Preface - How to Ask Questions](howtoaskquestions/Howtoaskquestions.md)
  - [Objectives](./objectives.md)
  - [Are you ready to ask a question?](howtoaskquestions/areyouready.md)
  - [Where to ask your question](howtoaskquestions/wheretoask.md)
  - [How to ask your question](howtoaskquestions/howtoask.md)
  - [Question Scavenger Hunt](Labs/QuestionScavengerHunt.md)

- [Agile](README.md)
  - [Objectives](./objectives.md)
  - [Overview of Agile](Agile/Overview.md)
  - [Checkpoint](Agile/Overview_Checkpoints.md)
  - [Knowledge check](Agile/Overview_MC.md)
  - [Performance Labs](Labs/AgileLab1.md)
  
-----------

[References](./references.md)

[C-4102L-TM Agile - Part 1 ](./part_i.md)
