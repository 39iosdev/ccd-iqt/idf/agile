<!--- add agile / scrum picture --->

# Agile Overview

## **Objectives**

### **In this module, you will learn about:**
* Agile
* Scrum  
* The role of transparency in software development 

---

## Understanding Agile
[Agile 101](https://www.agilealliance.org/agile101/)
[Scrum 101](https://digital.ai/resources/agile-101/what-is-scrum)
[Mike Cohn and Mountain Goat Software](https://www.mountaingoatsoftware.com/agile)


### **Being Agile vs. doing Agile**

[Understanding 'being' Agile vs. 'doing' Agile](https://medium.com/@Intersog/how-being-agile-is-different-from-doing-agile-9098e8b679f1)

---

### **The 4 Values of the Agile Manifesto**

The Agile Manifesto is at [agilemanifesto.org](https://agilemanifesto.org/)



| Individuals and interactions  | over  | process and tools           |
|-------------------------------|-------|-----------------------------|
| working software              | over  | comprehensive documentation |
| customer collaboration        | over  | contract negotiation        |
| responding to change          | over  | following a plan            |


----


---

### **12 Agile Principles** 

[The 12 Agile Principles](https://agilemanifesto.org/principles.html)

- Our highest priority is to satisfy the customer through early and continuous delivery of valuable software.

- Welcome changing requirements, even late in development. Agile processes harness change for the customer's competitive advantage.  

- Deliver working software frequently, from a couple of weeks to a couple of months, with a preference to the shorter timescale.  

- Business people and developers must work together daily throughout the project.  

- Build projects around motivated individuals. Give them the environment and support they need, and trust them to get the job done.  

- The most efficient and effective method of conveying information to and within a development team is face-to-face conversation.  

- Working software is the primary measure of progress.  

- Agile processes promote sustainable development. The sponsors, developers, and users should be able to maintain a constant pace indefinitely.  

- Continuous attention to technical excellence and good design enhances agility.  

- Simplicity--the art of maximizing the amount of work not done--is essential.  

- The best architectures, requirements, and designs emerge from self-organizing teams.  

- At regular intervals, the team reflects on how to become more effective, then tunes and adjusts its behavior accordingly.  

---


### **Agile roles**
* product owner
* scrum master
* dev
  
---

## **Agile vs. Scrum**

[The Agile Umbrella](https://www.visual-paradigm.com/scrum/scrum-vs-waterfall-vs-agile-vs-lean-vs-kanban/)
![Agile is the overarching framework](./assets/agile-umbrella.png)

Cf. *the Agile subway [from the Agile Alliance](https://www.agilealliance.org/agile101/subway-map-to-agile-practices/)*  

---
# ***Scrum***
For more about Scrum visit the [Scrum Alliance](https://www.scrumalliance.org/about-scrum/overview)


---
## **Scrum Principles**
[Scrum Principles ](https://qatestlab.com/resources/knowledge-center/six-scrum-principles/)
- control
- self organization
- collaboration
- value-based prioritization
- time-boxing
- iterative development
  

---

#### **Three Pillars of Scrum**


[List the Three Pillars of Scrum](https://www.agilistix.com/scrum/three-pillars-of-scrum/)
- Transparency
- Inspection
- Adaptation


[About the Three Pillars of Scrum](https://medium.com/@cocapitzer/learning-agile-three-pillars-to-make-scrum-work-88a6972a0a7c)


---

## **Scrum values**
[The 5 Scrum values from the Scrum Alliance](https://www.scrumalliance.org/about-scrum/values)

* commitment
  * Everything is timeboxed
  * Dev team commits to delivering functionality 
  * Business/ product owner commits to leaving priorities alone during the sprint
* focus
* openness
* courage
* respect

---

## **Scrum team**
* product owner
* Scrum master 
* Dev team
---

## **Scrum Ceremonies**
* product backlog
* sprint planning
* daily scrum/ standup
* product backlog refinement
* sprint review
* sprint retrospective


---

## **Scrum artifacts**
* product backlog
<!--- provide graphics from 90th --->
* sprint backlog
* increment


---

---

## **The Sprint**

  * Everything is timeboxed

  * Definition of done
    * cf. potentially shippable
    * Example definitions of done

---


### **Product backlog**
* Creating the backlog
* Refining the backlog

#### **Product backlog responsibilities**
* Product owner
  * create
  * maintain
  * prioritize
* Dev team
  * estimate 
  * understand
  * implement
* Scrum master
  * educate
  * coach
  
#### **Product backlog item attributes**
***compare with user stores https://www.mountaingoatsoftware.com/agile/user-stories***

| Title              | description                                         | estimate                         | acceptance criteria |
|--------------------|-----------------------------------------------------|----------------------------------|---------------------|
| example            | As a &lt;role&gt;, do something for some reason           | how much effort 1,2,3,5,8,13,... | when is it done?    |
| remote shell       | As a user,  I can invoke a shell on a remote system |                                  |                     |
| remote access tool | As a user,                                          |                                  |                     |
|                    |                                                     |                                  |                     |


---
## **Roles**
---
### **Dev Team** 

#### **Dev team _rights_**
* teams provide their own estimates
* sign up for work (not assigned)
* produce quality work
* ?? visibility in product backlog

### **Dev team _responsibilities_**
* perform the work on the _sprint_ backlog
* produce a _potentially_ deliverable product each sprint cf. *minimum viable product*
* determine their own practices
* estimate when asked
* pursue continuous improvement

### **Dev team _characteristics_**
* Teams are self organizing
* Teams are cross functional

---
### **Product owner** 

#### **Product owner _rights_**
* define priorities of a product
* decide when to release 
* produce quality work
* makes decisions

### **Product owner _responsibilities_**
* maintains and communicates vision
* clearly express and prioritize product backlog items
* manage stakeholders
* budget
* success

---
### **Scrum master** 

#### **Scrum master  _rights_**
* have access to stake-holders/decision makers
* address issues openly


### **Scrum master _responsibilities_**
* Scrum master is a *facilitator*
* ensures scrum is understood
* coach dev team on self organization
* coach product owner 
* collaborate with other scrum masters
* improve transparency
* be a servant leader 

### **Scrum master _characteristics_**
* facilitator
* servant leader
  * listens
  * coach not control
  * self aware 
  * bolsters others

---

# Ceremonies / Meetings 


- Daily Scrum
- Sprint Review / Sprint Planning 
- Retrospective
- daily scrum
- sprint planning
- retrospective
- 
---
# **Daily Scrum**
[What is the daily scrum?](https://www.scrum.org/resources/what-is-a-daily-scrum)
- What you accomplished
- What you plan to accomplish
- Potential blockers* 


---
# **Sprint Planning**
* Start with product backlog
* Finish with:
  * Sprint goal
  * _Sprint_ backlog


---
# ***Retrospective***
[The Retrospective explained](https://apiumhub.com/tech-blog-barcelona/agile-retrospective-ideas/)


---
# Practice estimating relative effort



  

---
# User Stories
[User stories](https://www.mountaingoatsoftware.com/agile/user-stories)

--- 



## **Scrum advantages** 


---

## **Important Agile/Scrum terms**
* sprint
* backlog
* deliverable
* acceptance criteria
* user story
* anti-patterns
* technical debt
* burndown chart
* story points 
* timebox
* definition of done
* velocity 
  

 *See more at* [the Agile Alliance](https://www.agilealliance.org/agile101/agile-glossary/)




---



## Further references
[Agile Tools ](https://apiumhub.com/tech-blog-barcelona/agile-project-management-tools/)

[A list of Agile Blogs](https://apiumhub.com/tech-blog-barcelona/top-agile-blogs/)
